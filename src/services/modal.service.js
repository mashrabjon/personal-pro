import Vue from 'vue'


const modalTemplate =
  `<b-modal id="bv-modal" :hide-footer="hideFooter"	@hide="onHide" @keydown.esc="modalOptions.persistent ? () => {} : closeModal()" :persistent="modalOptions.persistent" scrollable v-model="showDialog"  :max-width="typeof modalOptions.maxWidth == 'number' ? (modalOptions.maxWidth + 'px') : modalOptions.maxWidth">` +

  `<template v-slot:modal-title>` +
  `{{modalData.title}}` +
  `</template>` +
  `<template v-slot:default>` +
  `<ModalComponent :modal-data="modalData" :show-dialog.sync="showDialog"/>` +
  `</template>` +

  `</b-modal>`

export default {
  open(modalComponent, modalData, modalOptions = {
    maxWidth: '50vw',
    persistent: false
  }) {
    return new Promise(resolve => {
      // declares new passed Component to show
      let extendedModalComponent = Vue.extend(modalComponent).extend({
        props: {
          showDialog: Boolean,
          modalData: Object | undefined
        },
        methods: {
          closeModal(data) {
            this.$emit('update:showDialog', false)
            resolve(data)
          }
        }
      })
      // modal component
      let tempComponent = Vue.component('modal-component', {
        created() {},
        data() {
          return {
            showDialog: true,
            hideFooter: true,
            modalData: modalData,
            modalOptions: modalOptions,
            styles: {
              'padding': '16px',
              'box-shadow': '0 2px 4px 0 rgba(38,38,38,.08) !important',
              'background': 'white'
            }
          }
        },
        components: {
          ModalComponent: extendedModalComponent
        },
        methods: {
          closeModal(data) {
            this.showDialog = false
            resolve(data)
          },
          onHide(evt) {
            //console.log(evt.trigger);
            if (evt.trigger === "backdrop" || evt.trigger === "headerclose") {
              if (!confirm('$t{messages.confirm_exit}??')) {
                evt.preventDefault()
              }
            }
          }
        },
        template: modalTemplate
      })
      let ModalInstance = new tempComponent()
      ModalInstance.$mount()
    })
  }
}
