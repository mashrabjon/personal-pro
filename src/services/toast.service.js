const ToastService = {
  openSuccessDialog(toast, message, title) {
    toast.toast(message, {
      title: `${title || 'Message'}`,
      variant: 'success',
      solid: true
    })
  },
  openErrorDialog(toast, message, title) {
    toast.toast(message, {
      title: `${title || 'Message'}`,
      variant: 'danger',
      solid: true
    })
  },
  openWarningDialog(toast, message, title) {
    toast.toast(message, {
      title: `${title || 'Message'}`,
      variant: 'warning',
      solid: true
    })
  }
}
export default ToastService;
