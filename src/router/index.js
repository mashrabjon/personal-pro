import Vue from "vue";
import Router from "vue-router";
import TokenService from "./../services/storage.service";
import MainService from "./../services/main.service";
import store from './../store/store';


//import SocketService from "../services/socket.service";
// Containers
const DefaultContainer = () => import("@/containers/DefaultContainer");

// Views
const LoginPage = () => import("@/views/Login");
const Dashboard = () => import("@/views/Dashboard");
const HomePage = () => import("@/views/Home");
const Colors = () => import("@/views/theme/Colors");
const Typography = () => import("@/views/theme/Typography");

// Users
const User = () => import("@/views/users/Users");
// const User = () => import("@/views/admin/Users");

// Admin
const Users = () => import("@/views/admins/Users");
const Roles = () => import("@/views/admins/Roles");
const Menus = () => import("@/views/admins/Menus");
const Icons = () => import("@/views/admins/Icons");
const IconTypes = () => import("@/views/admins/IconTypes");
//Pages
const Page404 = () => import("@/views/pages/Page404");

// Chat
const ChatBoard = () => import("@/views/chat/board");

Vue.use(Router);

const router = new Router({
  mode: "history", // https://router.vuejs.org/api/#mode
  linkActiveClass: "open active",
  scrollBehavior: () => ({
    y: 0
  }),
  routes: [{
      path: "/",
      redirect: "/home",
      name: "Home",
      component: DefaultContainer,
      children: [{
          path: "home",
          name: "Home Page",
          component: HomePage
        },
        {
          path: "dashboard",
          name: "Dashboard",
          component: Dashboard
        },
        {
          path: "chat",
          name: "Chat",
          component: ChatBoard
        },
        {
          path: "theme",
          redirect: "/theme/colors",
          name: "Theme",
          component: {
            render(c) {
              return c("router-view");
            }
          },
          children: [{
              path: "colors",
              name: "Colors",
              component: Colors
            },
            {
              path: "typography",
              name: "Typography",
              component: Typography
            }
          ]
        },

        {
          path: "users",
          meta: {
            label: "Users"
          },
          component: {
            render(c) {
              return c("router-view");
            }
          },
          children: [{
              path: "",
              component: Users
            },
            {
              path: ":id",
              meta: {
                label: "User Details"
              },
              name: "User",
              component: User
            }
          ]
        },
        {
          path: "admin",
          redirect: "/admin/users",
          name: "Admin",
          component: {
            render(c) {
              return c("router-view");
            }
          },
          children: [{
              path: "users",
              name: "Users",
              component: Users
            },
            {
              path: "roles",
              name: "Roles",
              component: Roles
            },
            {
              path: "menus",
              name: "Menus",
              component: Menus
            },
            {
              path: "icons",
              name: "Icons",
              component: Icons
            },
            {
              path: "icontype",
              name: "Icon Types",
              component: IconTypes
            }
          ]
        }
      ]
    },
    {
      path: "/login",
      name: "Login",
      component: LoginPage,
      meta: {
        public: true, // Allow access to even if not logged in
        onlyWhenLoggedOut: true
      }
    },
    {
      path: "*",
      name: 'Page Not Found',
      component: Page404
    }
  ]
});

router.beforeEach(async (to, from, next) => {

  const isPublic = to.matched.some(record => record.meta.public);
  const onlyWhenLoggedOut = to.matched.some(record => record.meta.onlyWhenLoggedOut);

  //const loggedIn = !!TokenService.getToken();
  const isLoggedIn = !TokenService.isTokenExpired();

  // console.log({
  //   isLogged: isLoggedIn
  // });
  // console.log(isPublic, onlyWhenLoggedOut, isLoggedIn);

  if (!isPublic && !isLoggedIn) {
    return next({
      path: "/login",
      query: {
        redirect: to.fullPath
      } // Store the full path to redirect the user to after login
    });
  }
  if (isLoggedIn && onlyWhenLoggedOut) {
    //TODO call load functions

    return next("/");
  }
  // page refresh call
  if (!store.getters["dicts/isAllSet"]) {
    if (!TokenService.isTokenExpired()) {
      await MainService.loadAllPageRefresh();
    }
  }

  next();

});

export default router;
