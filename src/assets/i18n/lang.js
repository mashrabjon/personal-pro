import VueI18n from 'vue-i18n'
import Vue from "vue";

//import language files
import en from './pack/en.json';
import ru from './pack/ru.json';
import uz from './pack/uz.json';
import uzkr from './pack/uz-kr.json';

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: 'ru',
  //formatter: new CustomFormatter(/* here the constructor options */),
  fallbackLocale: 'ru',
  silentFallbackWarn: true,
  messages: {
    en,
    ru,
    uz,
    uzkr
  }
})

export default i18n;
