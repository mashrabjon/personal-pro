// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import "core-js/es6/promise";
import "core-js/es6/string";
import "core-js/es7/array";
// import cssVars from 'css-vars-ponyfill'
import Vue from "vue";
import BootstrapVue from "bootstrap-vue";
import Vuelidate from "vuelidate";

import App from "./App";
import router from "./router";
import store from "./store/store";
import ApiService from "./services/api.service";
import ServicesPlugin from "./plugins/ServicePlugin";
import i18n from './assets/i18n/lang';


Vue.use(BootstrapVue);
Vue.use(ServicesPlugin);
Vue.use(Vuelidate);

ApiService.init(process.env.VUE_APP_BASE_URL);

//axios.defaults.baseURL = 'http://localhost:4000/';
// If token exists set header

ApiService.mount401Interceptor();

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  store,
  i18n,
  template: "<App/>",
  components: {
    App
  }
});
