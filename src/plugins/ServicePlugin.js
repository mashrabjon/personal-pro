import ModalService from './../services/modal.service'

const ServicesPlugin = {
  install(Vue) {
    // services
    Vue.prototype.$modalService = ModalService
  }
}

export default ServicesPlugin
