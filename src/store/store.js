import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

import {
  auth
} from "./auth.module";
import {
  dicts
} from "./dicts.module";
import {
  socket
} from "./socket.module";
import {
  common
} from './common.module';

Vue.use(Vuex);
Vue.use(axios);

const store = new Vuex.Store({
  namespaced: true,

  modules: {
    auth,
    dicts,
    socket,
    common
  },
  state: {},
  getters: {},
  mutations: {},
  actions: {
    sampleAction() {
      console.log("action running...");
    }
  }
});
//
export default store;
