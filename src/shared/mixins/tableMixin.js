export default {
  data() {
    return {

    }
  },
  computed: {
    totalRows: function () {
      return this.getRowCount();
    },
    captions: function () {
      return this.fields;
    }
  },
  methods: {
    getBadge(status) {
      return status === "Active" ? "success" : "secondary";
    },
    getRowCount: function () {
      return this.itemsArray.length;
    },
    onRowSelected(item) {
      this.selected = item;
    },
    selectAllRows() {
      this.$refs.selectableTable.selectAllRows();
    },
    clearSelected() {
      this.$refs.selectableTable.clearSelected();
    },
    isSelected() {
      if (this.selected.length > 0) return true;
      else return false;
    }
  },
  watch: {

  }

};
