'use strict'

module.exports = {
  lintOnSave: false,
  runtimeCompiler: true,
  publicPath: '/',
  devServer: {
    //open: process.platform === 'darwin',
    host: 'localhost',
    port: 3000,
    https: false,
    hotOnly: false
    //proxy: 'http://10.9.0.17:42897'
  }
}
